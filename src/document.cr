require "yaml"
require "./enum"

module SynergiesGenerator
  module NumberExt
    def round_without_trailing_zero
      r = round
      return r.to_i if r.to_i == r
      r
    end
  end

  module ObjectExt
    def to_html(io : IO)
      to_s
    end

    def to_html
      String.build &->to_html(IO)
    end
  end

  struct Item
    LINK_NAME_OVERRIDES = {
      "Hero Pig"      => "Pig",
      "Cog Of Battle" => "Cog of Battle",
    }

    SPRITE_NAME_OVERRIDES = {
      "Grey Mauser" => "Grey Mauser (Gun)",
      "Bullet"      => "Bullet (Gun)",
    }

    YAML.mapping({
      name: String,
      id:   Int32,
    })

    def link_name
      LINK_NAME_OVERRIDES[@name]? || @name
    end

    def sprite_name
      SPRITE_NAME_OVERRIDES[@name]? || link_name
    end

    def to_s(io)
      io << @name
    end

    def to_html(io)
      io.puts "<th>"
      io << "[[File:" << sprite_name << ".png|32px|link=" << link_name << "]]<br>"
      io.puts "</th>"
    end
  end

  struct StatModifier
    YAML.mapping({
      stat:      Stat,
      operation: Operation,
      amount:    Float64,
    })

    def to_s(io)
      case @operation
      when Operation::Additive
        if @amount != 0
          io << "Increases " if @amount > 0
          io << "Decreases " if @amount < 0

          io << @stat << " "
          io << "by " << @amount.abs.round_without_trailing_zero << "."
        end
      when Operation::Multiplicative
        if @amount != 1
          io << "Increases " if @amount > 1
          io << "Decreases " if @amount < 1

          io << @stat << " "
          io << "by " << ((@amount - 1) * 100).abs.round_without_trailing_zero << "%."
        end
      end
    end
  end

  struct ItemCollection
    @or : Bool
    @items : Array(Item)

    def initialize(@items, @or = false)
    end

    def to_s(io)
      io << @items.join @or ? " or " : " and "
    end

    def to_html(io)
      if @items.size == 0
        io.puts "<p> None </p>"
        return
      end

      if @items.size >= 4
        io.puts "<p> Any one of the following: </p><br>" if @or
        @items.each_slice(4) do |slice|
          io.puts %(<table style="margin: 0px auto;">)
          io.puts "<tr>"
          slice.each do |e|
            e.to_html(io)
          end
          io.puts "</tr>"
          io.puts "<tr>"
          slice.each do |e|
            io.puts %(<td style="text-align: center">)
            io << "[[" << e.link_name << "|" << e.name << "]]\n"
            io.puts "</td>"
          end
          io.puts "</tr>"
          io.puts "</table>"
        end
      else
        io.puts %(<table style="margin: 0px auto;">)

        io.puts "<tr>"
        @items.each_with_index do |e, i|
          e.to_html(io)
          if @or && i < (@items.size - 1) # if not last element
            io.puts "<th>"
            io.puts "or"
            io.puts "</th>"
          end
        end
        io.puts "</tr>"

        io.puts "<tr>"
        @items.each_with_index do |e, i|
          io.puts %(<td style="text-align: center">)
          io << "[[" << e.link_name << "|" << e.name << "]]\n"
          io.puts "</td>"
          if @or && i < (@items.size - 1) # if not last element
            io.puts "<td></td>"
          end
        end
        io.puts "</tr>"

        io.puts "</table>"
      end
    end

    macro method_missing(call)
      {% if call.block %}
        @items.{{call.name.id}}({{call.args.splat}}) do |{{call.block.args.splat}}|
          {{call.block.body}}
        end
      {% else %}
        @items.{{call.name.id}}({{call.args.splat}})
      {% end %}
    end
  end

  struct Synergy
    UNUSED_ITEMS = ["M9"]

    @items : ItemCollection? = nil
    @guns : ItemCollection? = nil

    YAML.mapping({
      items_or:  {getter: false, setter: false, type: Bool},
      guns_or:   {getter: false, setter: false, type: Bool},
      stat_mods: Array(StatModifier),
      _items:    {getter: false, setter: false, type: Array(Item), key: "items"},
      _guns:     {getter: false, setter: false, type: Array(Item), key: "guns"},
      bonus:     Array(BonusSynergy),
    })

    def items
      @items ||= ItemCollection.new items: @_items, or: @items_or
    end

    def guns
      @guns ||= ItemCollection.new items: @_guns, or: @guns_or
    end

    def unused
      items.any? { |x| UNUSED_ITEMS.includes? x.name } ||
        guns.any? { |x| UNUSED_ITEMS.includes? x.name }
    end

    def to_s(io)
      io << items.to_s if items.size > 0
      io << " & " if items.size > 0 && guns.size > 0
      io << guns.to_s if guns.size > 0
      io << ": "
      io << stat_mods.join(" ")
      io << bonus.join(" ")
    end

    def to_html(io)
      return if unused

      io.puts "<tr>"
      io.puts "<td>"
      items.to_html(io)
      io.puts "</td>"

      io.puts "<td>"
      guns.to_html(io)
      io.puts "</td>"

      io.puts "<td>"
      stat_mods_desc = stat_mods.join(" ")
      bonus_desc = bonus.join(" ")
      if stat_mods_desc == "" && bonus_desc == ""
        io << "No effect."
      else
        io << stat_mods_desc << " " << bonus_desc
      end
      io.puts "</td>"
      io.puts "</tr>"
    end
  end

  struct Document
    YAML.mapping({
      synergies: Array(Synergy),
    })

    def to_html(io)
      io.puts <<-END
        [[File:Synergy.png|33px]]

        <p>
        Some items will produce a unique effect when held at the same time.
        A light blue arrow will appear above players for a short duration to show that a special combo is available.
        </p>

        <!--
        This page was autogenerated using https://gitlab.com/Zatherz/gungeon-wiki-synergy-page-generator.
        -->

        END

      io.puts <<-END
        <table style="text-align:center; width: 75%" class="wikitable sortable" >
          <tr>
            <th style="width: 30%">Items</th>
            <th style="width: 30%">Guns</th>
            <th style="width: 40%">Effect</th>
          </tr>
        END

      synergies.each &.to_html(io)

      io.puts "</table>"
    end
  end
end

struct Number
  include SynergiesGenerator::NumberExt
end

class Object
  include SynergiesGenerator::ObjectExt
end
