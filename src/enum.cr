module SynergiesGenerator
  enum Operation
    Additive
    Multiplicative
  end

  enum BonusSynergy
    CopVest
    ZombieAmmo
    DemonHunter
    BulletKinKin
    MetroidCanCrawl
    UnbelievablyCharming
    BeAChicken
    TheCoinKing
    Downerwell

    def to_s
      case self
      when CopVest
        "Halves damage taken by the Cop."
      when ZombieAmmo
        "Doubles the chance to get ammo back from Zombie Bullets while using the synergized guns."
      when DemonHunter
        "Causes the synergized guns to deal 50% more damage to jammed enemies, for a total of +487.5% with the bonus from Silver Bullets."
      when BulletKinKin
        "" # unused
      when MetroidCanCrawl
        "Causes Roll Bombs to spawn normal explosions (as opposed to small ones)."
      when UnbelievablyCharming
        "All bullets shot from the synergized guns have a 100% chance to charm enemies on hit."
      when BeAChicken
        "Increases the chance for bullets shot from the synergized guns to transmogrify enemies."
      when TheCoinKing
        "Doubles the chance for a money drop from Coin Crown."
      when Downerwell
        "Removes Gun Boots' cooldown."
      end
    end
  end

  enum Stat
    MovementSpeed
    RateOfFire
    Spread
    Health
    Coolness
    Damage
    ProjectileSpeed
    GunCapacity  # Additional...
    ItemCapacity # Additional...
    TheAmmoCapacityMultiplier
    ReloadTime
    TheAmountOfTimesAProjectileCanPierceThroughEnemies # Additional...
    TheKnockbackMultiplier
    TheGlobalPriceMultiplier
    Curse
    PlayerBulletScale
    TheClipCapacityMultiplier            # Additional...
    TheAmountOfTimesAProjectileCanBounce # Additional...
    BlanksPerFloor                       # Additional...
    ShadowBulletChance
    ThrownGunDamage
    DodgeRollDamage
    DamageToBosses
    TheEnemyProjectileSpeedMultiplier
    ExtremeShadowBulletChance
    TheChargeAmountMultiplier
    TheRangeMultiplier

    def to_s(io)
      io << to_s.gsub(/([A-Z])/, " \\1").strip.downcase
    end
  end
end
